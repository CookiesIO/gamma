#pragma semicolon 1

#include <sourcemod>
#include <sdktools>

#pragma newdecls required
#include <gamma>

#define PLUGIN_VERSION "2.1"

public Plugin myinfo = 
{
	name = "Basic TF2 Round Manager",
	author = "Cookies.io",
	description = "A basic TF2 Round Manager for Gamma",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public void OnPluginStart()
{
	CreateConVar("tf2_roundmanager_version", PLUGIN_VERSION, "Basic TF2 Round Manager Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
}

public void OnAllPluginsLoaded()
{
	AddDefinitionCreatedListener(GAMMA_ROUNDMANAGER_DEFINITION, OnRoundManagerDefinitionCreated);
}

public void OnPluginEnd()
{
	// One should always destroy all ones definitions, otherwise bad stuff happens
	DestroyMyDefinitions();
}

public void OnRoundManagerDefinitionCreated(Definition definition)
{
	RoundManagerCreator creator = new RoundManagerCreator("Basic TF2 Round Manager", OnTakeControl, OnLostControl, OnForceEndRound);
	creator.Finalize();
}

public void OnTakeControl(bool startActive)
{
	HookEvent("teamplay_round_start", Event_RoundStart, EventHookMode_Pre);
}

public void OnLostControl()
{
	UnhookEvent("teamplay_round_start", Event_RoundStart, EventHookMode_Pre);
}

public void OnForceEndRound()
{
	int gameRoundWin = FindEntityByClassname(-1, "game_round_win");
	if (gameRoundWin == -1)
	{
		gameRoundWin = CreateEntityByName("game_round_win");
		if (gameRoundWin == -1)
		{
			ThrowError("No game_round_win entity could be found or created.");
			return;
		}
		DispatchSpawn(gameRoundWin);
	}

	// 0 == stalemate
	SetVariantInt(0);
	AcceptEntityInput(gameRoundWin, "SetTeam");
	AcceptEntityInput(gameRoundWin, "RoundWin");
}

public void OnMapEnd()
{
	if (Gamma_IsActive())
	{
		Gamma_RoundEnd();
	}
}

public void Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	if (Gamma_IsActive())
	{
		Gamma_RoundEnd();
	}
	Gamma_RoundStart();
}