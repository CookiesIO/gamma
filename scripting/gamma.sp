#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <definitions>
#include <gamma>

/*********************************************************************
 *
 *  Gamma is made to easily develop game modes that can coexist with
 *  other game modes, and also be extended by thirdparties.
 *
 *  In my original version of Gamma I had something called behaviors,
 *  they could be defined by game modes, implemented by 3rd party
 *  plugins, and assigned to players by the game mode. However, I
 *  have decided to remove such a feature from v2, due to the
 *  definitions library backing Gamma, which makes it possible to
 *  make things that fits exactly your needs.
 *
 *  However, it could be possible to write a library that could make
 *  such functionality available from outside of gamma itself!
 *
 *  As you can see, a barebones game mode manager doesn't have to be
 *  huge or overly complex to support multiple games, nor to support
 *  a wide variety of game modes.
 *
 *  Nothing is stopping you from writing a legacy game mode plugins,
 *  that loads a config with commands and cvars and then uses gamma
 *  to tell when to toggle game modes on or off! For example using a
 *  configuration like, to generate game mode definitions:
 *  https://forums.alliedmods.net/showthread.php?p=2039152
 *
 * *******************************************************************
 *
 *  I hope that people will embrace the way of thinking poured into
 *  this project, to stay modular and flexible, so that all servers
 *  can offer an as unique experience as they want.
 *
 *  Nothing prevents anyone from coding in restrictions directly, but
 *  for the sake of everyones best interests, provide it in external
 *  plugins, in such a way that it could work for any game mode, a
 *  good example: Weapon Restriction. 
 *
 *  I've seen these hardcoded into VSH/FF2 and it makes me cringe,
 *  these are not neccesarily required by every server and would
 *  benefit a lot if they were moved to another plugin, and made
 *  config based. Nothing stops you from distributing default configs,
 *  but at least this way people can change them as they wish.
 *  And it could even offer per game mode restrictions.
 *
 * *** **** Definitions ***** Gamma ***** Boss Fight Fortress **** ***
 *
 *********************************************************************/

#define PLUGIN_VERSION "2.0"

public Plugin myinfo = 
{
	name = "Gamma",
	author = "Cookies.io",
	description = "Extensible Game Mode Manager based on Definitions",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

Definition g_hRoundManagerDefinition;
Definition g_hGameModeDefinition;

ConVar g_hIsEnabledVar;            // gamma_enabled            "<0|1>"
ConVar g_hGameModeVar;             // gamma_gamemode           "gamemode name (name of definition based on Gamma_GameMode)"
ConVar g_hGameModeSelectionVar;    // gamma_gamemode_selection "<1|2|3>"
ConVar g_hRoundManagerVar;         // gamma_roundmanager       "round manager name"

bool g_bIsEnabled;
Definition g_hCurrentMapGameMode;

bool g_bIsActive;
Definition g_hCurrentGameMode;
Definition g_hCurrentRoundManager;

Handle g_hOnGameModeStarted;
Handle g_hOnGameModeEnded;

public APLRes AskPluginLoad2()
{
	CreateNative("Gamma_IsActive", Native_IsActive);
	CreateNative("Gamma_GetCurrentGameMode", Native_GetCurrentGameMode);
	CreateNative("Gamma_SetGameMode", Native_SetGameMode);
	CreateNative("Gamma_RoundStart", Native_RoundStart);
	CreateNative("Gamma_RoundEnd", Native_RoundEnd);
	CreateNative("Gamma_ForceEndGameMode", Native_ForceEndGameMode);

	g_hOnGameModeStarted = CreateGlobalForward("OnGameModeStarted", ET_Ignore, Param_Cell);
	g_hOnGameModeEnded = CreateGlobalForward("OnGameModeEnded", ET_Ignore, Param_Cell, Param_Cell);

	RegPluginLibrary("gamma");
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("gamma_version", PLUGIN_VERSION, "Gamma Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
	
	g_hIsEnabledVar = CreateConVar("gamma_enabled", "1", "Sets whether Gamma is enabled or not, is taken into account per map.", FCVAR_NOTIFY, true, 0.0, true, 1.0);
	g_hGameModeVar = CreateConVar("gamma_gamemode", "", "Sets the game mode which should be played, is taken into account per map", FCVAR_NOTIFY);
	g_hGameModeSelectionVar = CreateConVar("gamma_gamemode_selection", "3", "Sets the game mode selection mode, 1=Strictly by name, 2=First game mode able to start, 3=First try by name and if it fails then first able to start", FCVAR_NOTIFY, true, 1.0, true, 3.0);

	g_hRoundManagerVar = CreateConVar("gamma_roundmanager", "", "Sets your preferred Round Manager, if it doesn't exist the first available Round Manager will be chosen", FCVAR_NOTIFY);
	g_hRoundManagerVar.AddChangeHook(OnRoundManagerVarChanged);

	AutoExecConfig(true, "gamma");
}

public void OnPluginEnd()
{
	// REQUIRED! Else bad things can happen when unloading.. BAD! THINGS!
	DestroyMyDefinitions();
}

public void OnAllPluginsLoaded()
{
	DefinitionCreator creator = new DefinitionCreator(GAMMA_ROUNDMANAGER_DEFINITION);
	creator.DefineFunction("TakeControl");
	creator.DefineFunction("LostControl");
	creator.DefineFunction("ForceEndRound");
	creator.ForceTemplate();
	g_hRoundManagerDefinition = creator.Finalize();

	creator = new DefinitionCreator(GAMMA_GAMEMODE_DEFINITION);
	creator.SetFunction("CanStart", DefaultCanStart);
	creator.SetFunction("WorksInMap", DefaultWorksInMap);
	creator.SetFunction("SelectedForMap", DefaultSelectedForMap);
	creator.DefineFunction("Start");
	creator.DefineFunction("Ended");
	creator.ForceTemplate();
	g_hGameModeDefinition = creator.Finalize();

	// Make sure we get a round manager registered
	// Game Modes are searched through when required, so no special treatment for those
	RefreshRoundManager();
}

/******************************************
 *
 *      Definition defaults!
 *
 *****************************************/

// We're just going to assume people want to always be able to start if they don't specify it
public bool DefaultCanStart()
{
	return true;
}

// We're just going to assume people want to always be able to work in a map if they don't specify it
public bool DefaultWorksInMap(const char[] map)
{
	return true;
}

public void DefaultSelectedForMap()
{
}

/******************************************
 *
 *      GameMode stuff!
 *
 *****************************************/

// Be nice to other plugins <3
public void OnConfigsExecuted()
{
	g_bIsEnabled = g_hIsEnabledVar.BoolValue;
	if (g_bIsEnabled)
	{
		char map[PLATFORM_MAX_PATH];
		GetCurrentMap(map, sizeof(map));
		
		Definition gameMode = GetGameModeForMap(map);
		if (gameMode.IsValidEx(false, g_hGameModeDefinition))
		{
			g_hCurrentMapGameMode = gameMode;
			gameMode.StartFunction("SelectedForMap");
			Call_Finish();
		}
		else
		{
			g_hCurrentMapGameMode = null;
		}

		RefreshRoundManager();
	}
	else
	{
		g_hCurrentMapGameMode = null;
		ClearCurrentRoundManager();
	}
}

Definition GetGameModeForMap(const char[] map)
{
	// Selection modes explained:
	// 1 - Game mode strictly by name
	// 2 - Whatever game mode first found that can start, completely disregarding the game mode cvar
	// 3 - See if the game mode by name can start, if that fails then the first game mode that can start
	int selection = g_hGameModeSelectionVar.IntValue;
	if (selection == 1 || selection == 3)
	{
		char gameModeName[255];
		g_hGameModeVar.GetString(gameModeName, sizeof(gameModeName));
		Definition gameMode = FindDefinition(gameModeName);

		if (GameModeWorksInMap(gameMode, map))
		{
			return gameMode;
		}
		else if (selection == 1)
		{
			return null;
		}
	}

	ArrayList gameModes = g_hGameModeDefinition.GetChildren();
	int length = gameModes.Length;
	for (int i = 0; i < length; i++)
	{
		Definition gameMode = view_as<Definition>(gameModes.Get(i));
		if (GameModeWorksInMap(gameMode, map))
		{
			gameModes.Close();
			return gameMode;
		}
	}

	gameModes.Close();
	return null;
}

bool GameModeWorksInMap(Definition gameMode, const char[] map)
{
	if (gameMode.IsValidEx(false, g_hGameModeDefinition))
	{
		bool result;
		gameMode.StartFunction("WorksInMap");
		Call_PushString(map);
		Call_Finish(result);
		return result;
	}
	return false;
}

public void StartGameMode()
{
	if (g_bIsActive)
	{
		StopGameMode(GameModeEndReason_Ended);
	}

	if (g_bIsEnabled)
	{
		Definition gameMode = GetGameModeForRound();

		if (gameMode.IsValidEx(false, g_hGameModeDefinition))
		{
			g_hCurrentGameMode = gameMode;

			g_hCurrentGameMode.StartFunction("Start");
			Call_Finish();

			Call_StartForward(g_hOnGameModeStarted);
			Call_PushCell(g_hCurrentGameMode);
			Call_Finish();

			g_bIsActive = true;
		}
	}
}

public void StopGameMode(GameModeEndReason reason)
{
	if (g_bIsActive)
	{
		g_bIsActive = false;
		
		g_hCurrentGameMode.StartFunction("Ended");
		Call_PushCell(reason);
		Call_Finish();

		Call_StartForward(g_hOnGameModeEnded);
		Call_PushCell(g_hCurrentGameMode);
		Call_PushCell(reason);
		Call_Finish();

		// If there was a failure that happened that caused the game mode to end, force end the round
		if (reason == GameModeEndReason_Failure)
		{
			g_hCurrentRoundManager.StartFunction("ForceEndRound");
			Call_Finish();
		}

		g_hCurrentGameMode = null;
	}
}

Definition GetGameModeForRound()
{
	if (CanGameModeStart(g_hCurrentMapGameMode))
	{
		return g_hCurrentMapGameMode;
	}
	return null;
}

bool CanGameModeStart(Definition gameMode)
{
	if (gameMode.IsValidEx(false, g_hGameModeDefinition))
	{
		bool result;
		gameMode.StartFunction("CanStart");
		Call_Finish(result);
		return result;
	}
	return false;
}


/******************************************
 *
 *      RoundManager stuff!
 *
 *****************************************/
public void OnRoundManagerVarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	// Change roundmanager if the preferred round manager is changed, and the new preferred one exists
	if (g_hCurrentRoundManager.IsValid())
	{
		if (g_hCurrentRoundManager.IsDefinition(newValue) == false)
		{
			Definition definition = FindDefinition(newValue);
			if (definition.IsValidEx(false, g_hRoundManagerDefinition))
			{
				SetNewRoundManager(definition);
			}
		}
	}
}

Definition FindBestRoundManager()
{
	Definition bestRoundManager = null;

	char name[255];
	g_hRoundManagerVar.GetString(name, sizeof(name));

	ArrayList children = g_hRoundManagerDefinition.GetChildren();
	int length = children.Length;
	for (int i = 0; i < length; i++)
	{
		Definition definition = view_as<Definition>(children.Get(i));
		if (bestRoundManager.IsValid() == false)
		{
			bestRoundManager = definition;
		}
		else
		{
			if (definition.IsDefinition(name))
			{
				bestRoundManager = definition;
				break;
			}
		}
	}
	children.Close();

	return bestRoundManager;
}

void RefreshRoundManager()
{
	Definition roundManager = FindBestRoundManager();
	SetNewRoundManager(roundManager);
}

void SetNewRoundManager(Definition definition)
{
	if (definition.IsValidEx(false, g_hRoundManagerDefinition))
	{
		// Make sure to clear the current before setting the new one!
		ClearCurrentRoundManager();
		g_hCurrentRoundManager = definition;
		g_hCurrentRoundManager.StartFunction("TakeControl");
		Call_Finish();
	}
}

void ClearCurrentRoundManager()
{
	if (g_hCurrentRoundManager.IsValid())
	{
		g_hCurrentRoundManager.StartFunction("LostControl");
		Call_Finish();
		g_hCurrentRoundManager = null;
	}
}


/******************************************
 *
 *      Definition snooping!
 *
 *****************************************/
public void OnDefinitionCreated(Definition definition)
{
	if (definition.IsValidEx(false, g_hRoundManagerDefinition))
	{
		RefreshRoundManager();
	}
}

public void OnDefinitionDestroyed(Definition definition)
{
	if (definition == g_hCurrentMapGameMode)
	{
		g_hCurrentMapGameMode = null;
	}
	
	if (definition == g_hCurrentGameMode)
	{
		// Ew, we no likey when this happens :(
		StopGameMode(GameModeEndReason_Failure);
		LogError("Game Mode was destroyed while it was in use.");
	}
	else if (definition == g_hCurrentRoundManager)
	{
		Definition roundManager = FindBestRoundManager();
		if (roundManager.IsValidEx(false, g_hRoundManagerDefinition))
		{
			// This we like!
			SetNewRoundManager(roundManager);
		}
		else
		{
			// We don't like this :(
			StopGameMode(GameModeEndReason_Failure);
			ClearCurrentRoundManager();
			LogError("The round manager was destroyed while it was in use, and there was no replacement to take over it's job.");
		}
	}
}


/******************************************
 *
 *      Natives!
 *
 *****************************************/
public int Native_IsActive(Handle plugin, int numParams)
{
	return g_bIsActive;
}

public int Native_GetCurrentGameMode(Handle plugin, int numParams)
{
	return view_as<int>(g_hCurrentGameMode);
}

public int Native_SetGameMode(Handle plugin, int numParams)
{
	Definition gameMode = view_as<Definition>(GetNativeCell(1));
	if (gameMode.IsValidEx(false, g_hGameModeDefinition))
	{
		int bufferSize = gameMode.NameLength + 1;
		char[] name = new char[bufferSize];
		gameMode.GetName(name, bufferSize);
		g_hGameModeVar.SetString(name);
		return true;
	}
	return false;
}

public int Native_RoundStart(Handle plugin, int numParams)
{
	if (g_hCurrentRoundManager.IsValid(false) && g_hCurrentRoundManager.IsFromPlugin(plugin))
	{
		StartGameMode();
	}
	else
	{
		ThrowNativeError(SP_ERROR_NATIVE, "Gamma_RoundStart can only be called from the currently active Round Manager");
	}
}

public int Native_RoundEnd(Handle plugin, int numParams)
{
	if (g_hCurrentRoundManager.IsValid(false) && g_hCurrentRoundManager.IsFromPlugin(plugin))
	{
		StopGameMode(GameModeEndReason_Ended);
	}
	else
	{
		ThrowNativeError(SP_ERROR_NATIVE, "Gamma_RoundEnd can only be called from the currently active Round Manager");
	}
}

public int Native_ForceEndGameMode(Handle plugin, int numParams)
{
	if (g_hCurrentGameMode.IsValid(false) && g_hCurrentGameMode.IsFromPlugin(plugin))
	{
		StopGameMode(GameModeEndReason_Failure);
	}
	else
	{
		ThrowNativeError(SP_ERROR_NATIVE, "Gamma_ForceEndGameMode can only be called from the currently active game mode");
	}
}