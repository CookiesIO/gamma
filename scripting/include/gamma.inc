#if defined _GAMMA_INCLUDED
  #endinput
#endif
#define _GAMMA_INCLUDED

// Gamma heavily relies on definitions, so might as well provide helpers!
#include <definitions>

#define GAMMA_GAMEMODE_DEFINITION "Gamma_GameMode"
#define GAMMA_ROUNDMANAGER_DEFINITION "Gamma_RoundManager"

enum GameModeEndReason
{
	GameModeEndReason_Ended,
	GameModeEndReason_Failure
}

/**
 * typedefs for GameModes
 **/
typedef GameModeCanStart = function bool();
typedef GameModeWorksInMap = function bool(const char[] map);
typedef GameModeSelectedForMap = function void();
typedef GameModeStart = function void();
typedef GameModeEnded = function void(GameModeEndReason reason);

/**
 * typedefs for RoundManagers
 **/
typedef TakeControl = function void(bool startActive);
typedef LostControl = function void();
typedef ForceEndRound = function void();

/**
 * Round manager methodmaps, for convenience
 **/
methodmap RoundManager < Definition
{
}

methodmap RoundManagerCreator < DefinitionCreator
{
	public RoundManagerCreator(const char[] name, TakeControl takeControl, LostControl lostControl, ForceEndRound forceEndRound)
	{
		Definition roundManager = FindDefinition(GAMMA_ROUNDMANAGER_DEFINITION);
		if (roundManager.IsValid())
		{
			DefinitionCreator creator = new DefinitionCreator(name, roundManager);
			creator.SetFunction("TakeControl", takeControl);
			creator.SetFunction("LostControl", lostControl);
			creator.SetFunction("ForceEndRound", forceEndRound);
			return view_as<RoundManagerCreator>(creator);
		}
		ThrowError("Make sure the Gamma_RoundManager definition exists before attempting to create new round managers!");
		// Statisfy the compiler.. :/
		return null;
	}

	public RoundManager Finalize()
	{
		DefinitionCreator creator = view_as<DefinitionCreator>(this);
		return view_as<RoundManager>(creator.Finalize());
	}
}

/**
 * Game mode methodmaps, for convenience
 **/
methodmap GameMode < Definition
{
}

methodmap GameModeCreator < DefinitionCreator
{
	public GameModeCreator(const char[] name, GameModeStart start, GameModeEnded ended, Definition parentGameMode=null)
	{
		Definition gammaGameMode = FindDefinition(GAMMA_GAMEMODE_DEFINITION);
		Definition gameMode = (parentGameMode.IsValid() ? parentGameMode : gammaGameMode);
		if (gammaGameMode.IsValid() && (gameMode == gammaGameMode || gameMode.IsValidEx(true, gammaGameMode)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, gameMode);
			creator.SetFunction("Start", start);
			creator.SetFunction("Ended", ended);
			return view_as<GameModeCreator>(creator);
		}
		ThrowError("Make sure the Gamma_GameMode definition exists before attempting to create new game modes!");
		// Statisfy the compiler.. :/
		return null;
	}

	public void SetCanStart(GameModeCanStart canStart)
	{
		this.SetFunction("CanStart", canStart);
	}

	public void SetWorksInMap(GameModeWorksInMap worksInMap)
	{
		this.SetFunction("WorksInMap", worksInMap);
	}

	public void SetSelectedForMap(GameModeSelectedForMap selectedForMap)
	{
		this.SetFunction("SelectedForMap", selectedForMap);
	}

	public GameMode Finalize()
	{
		DefinitionCreator creator = view_as<DefinitionCreator>(this);
		return view_as<GameMode>(creator.Finalize());
	}
}

/**
 * Gets whether a game mode is active or not
 *
 * @return  True if a game mode is currently active, false otherwise
 **/
native bool Gamma_IsActive();

/**
 * Gets the currently active game mode
 *
 * @return  Current game mode, null if no game mode is active
 **/
native GameMode Gamma_GetCurrentGameMode();

/**
 * Sets the game mode to use from now on
 *
 * @param gameMode  Game mode
 * @return          True if valid game mode, false otherwise
 **/
native bool Gamma_SetGameMode(GameMode gameMode);

/**
 * Notifies Gamma that a round has started
 *
 * @error  If not called from the current round manager plugin
 **/
native void Gamma_RoundStart();

/**
 * Notify Gamma that a round has ended
 *
 * @error  If not called from the current round manager plugin.
 **/
native void Gamma_RoundEnd();

/**
 * Notify Gamma to forcefully end the current game mode
 *
 * @error  If not called from the current game mode.
 **/
native void Gamma_ForceEndGameMode();

/**
 * Called when a game mode has started/ended
 **/
forward void OnGameModeStarted(GameMode gameMode);
forward void OnGameModeEnded(GameMode gameMode, GameModeEndReason reason);


/* DO NOT EDIT BELOW THIS LINE */
public SharedPlugin __pl_gamma = 
{
	name = "gamma",
	file = "gamma.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_gamma_SetNTVOptional()
{
	MarkNativeAsOptional("Gamma_IsActive");
	MarkNativeAsOptional("Gamma_GetCurrentGameMode");
	MarkNativeAsOptional("Gamma_SetGameMode");
	MarkNativeAsOptional("Gamma_RoundStart");
	MarkNativeAsOptional("Gamma_RoundEnd");
}
#endif