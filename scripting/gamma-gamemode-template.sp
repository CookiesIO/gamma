#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <gamma>

#define PLUGIN_VERSION "2.1"

public Plugin myinfo = 
{
	name = "Gamma Game Mode Template",
	author = "Cookies.io",
	description = "A template for Gamma Game Modes!",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

GameMode g_hMyGameMode;

public void OnPluginStart()
{
	CreateConVar("gamma_gamemode_template_version", PLUGIN_VERSION, "Gamma Game Mode Template Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
}

public void OnAllPluginsLoaded()
{
	AddDefinitionCreatedListener(GAMMA_GAMEMODE_DEFINITION, OnGameModeDefinitionAvailable);
}

public void OnPluginEnd()
{
	// REQUIRED! Else bad things can happen when unloading.. BAD! THINGS!
	DestroyMyDefinitions();
}

public void OnGameModeDefinitionAvailable(Definition definition)
{
	GameModeCreator creator = new GameModeCreator("A Game Mode Template", OnStart, OnEnded);
	creator.SetCanStart(OnCanStart);
	g_hMyGameMode = creator.Finalize();
}

public void OnDefinitionDestroyed(Definition definition)
{
	if (definition == g_hMyGameMode)
	{
		g_hMyGameMode = null;
	}
}

// optional!
public bool OnCanStart()
{
	PrintToServer("GameMode Template: Can i Start? YES I CAN!");
	return true;
}

// required!
public void OnStart()
{
	PrintToServer("GameMode Template: I got a green light to set up everything I need for my game mode!");
}

// required!
public void OnEnded(GameModeEndReason reason)
{
	PrintToServer("GameMode Template: The round is over, hopefully it was fun, but this is it for me for now. The reason for ending now was: %s.", (reason == GameModeEndReason_Ended ? "We finished what we were doing" : "A failure of sorts"));
}
